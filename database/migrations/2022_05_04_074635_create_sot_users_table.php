<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSotUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sot_users', function (Blueprint $table) {
            $table->id();         
            $table->string('firstname');
            $table->string('email')->unique();
            $table->string('gender');
            $table->string('avatar_name');
            $table->string('character');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sot_users');
    }
}
