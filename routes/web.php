<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/index2', function () {
    return view('index2');
});

Route::get('/input', function () {
    return view('input');
});

Route::get('/team', function () {
    return view('team1')->with(
        ['hideBeforeInit' => true]
    );
});

Route::get('/mobile', function () {
    return view('mobile');
});

//Route::get('/mails', [MailController::class,'index'])->middleware(['auth']);

Route::get('/select', function () {
    return view('select');
});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    })->name('dashboard');
    Route::get('/select-avatar', function() {
        return view('admin.select-avatar');
    });
    Route::get('/mails', [MailController::class,'index']);
});


require __DIR__.'/auth.php';
