<?php

namespace App\Http\Controllers;

use App\Models\SotUser;
use Illuminate\Http\Request;

class SotUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'firstname' => 'required|string',
            'email' => 'required|email|unique:sot_users',            
            'avatar_name' => 'required|string',
            'character' => 'required|string',
        ]);
               
        $sotUser = SotUser::create([
            'firstname' => $request->firstname, 
            'email' => $request->email,  
            'gender' => $request->character == 'girl' ? 'f' : 'm', 
            'avatar_name' => $request->avatar_name, 
            'character' => $request->character
        ]);
        return response()->json($sotUser);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SotUser  $sotUser
     * @return \Illuminate\Http\Response
     */
    public function show(SotUser $sotUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SotUser  $sotUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SotUser $sotUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SotUser  $sotUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(SotUser $sotUser)
    {
        //
    }
}
