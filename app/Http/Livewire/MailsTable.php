<?php

namespace App\Http\Livewire;

use App\Models\Mail;
use Livewire\Component;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class MailsTable extends DataTableComponent
{

    protected $model = Mail::class;

    public function configure(): void {        
        $this->setPrimaryKey('id');
    }
    
    public function columns(): array
    {
        return [   
            Column::make('Name', 'name')->sortable()
            ->searchable(),             
            Column::make('E-mail', 'email')->sortable()
            ->searchable(),            
            Column::make('Created', 'created_at')->sortable()
            ->searchable(),
        ];
    }    
}
