<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SotUser extends Model
{
    use HasFactory;

    protected $table = 'sot_users';
    protected $fillable = ['firstname', 'email','gender','avatar_name','character'];        
    protected $primaryKey = 'id';     
    
        /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

}
