@extends('layout.master')
@section('header_includes')
<link rel="stylesheet" href="/css/slippry.css">
<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="/js/slippry.min.js"></script>
<style>
    .menu a,
    .menu .fab {
        color: #E53352;
    }

    .trans_right {

        padding: 2rem;
        width: 13%;
        float: right;
        background: rgba(71, 67, 255, 0.9);
        height: 65%;
    }

    .trans_left {

        padding: 2rem;
        width: 13%;
        float: left;
        background: rgba(71, 67, 255, 0.9);
        height: 65%;
    }

</style>
@endsection

@section('content')
<div class="overflow-hidden relative">
    <picture class="">
        <source media="(max-width: 480px)" sizes="100vw" srcset="img/team/bg-mobile.png 1x">
        <source media="(max-width: 1024px)" sizes="100vw" srcset="img/team/bg-ipad.png 1x, img/team/bg-ipad@2x.png 2x">
        <source media="(max-width: 1920px)" sizes="100vw" srcset="img/team/bg.jpg 1x, img/team/bg@2x.jpg 2x">
        <img class="img-fluid img-header" src="img/team/bg.jpg" alt="bg" style="width: 100%;height: auto;">
    </picture>

    <div id="team" class="w-full absolute top-1/2 transform -translate-y-1/2 left-1/3 ml-16">
        <div class="sm:max-w-lg lg:max-w-4xl">
            <ul class="wrapper mw-800:w-[80%] text-xs text-white text-right">
                <li id="demo1" class="flex flex-col gap-y-8">
                    <div class="sm:grid sm:grid-cols-4">
                        <div class="col-span-1 flex flex-col items-end">
                            <img class="max-w-full" src="/img/team/scott.png" alt="Scott O.">
                            <span>Founder | Creative Director</span>
                        </div>
                        <div class="col-span-1 flex flex-col items-end">
                            <img class="max-w-full" src="img/team/armond.png" alt="Armond A.">
                            <span>CFO</span>
                        </div>
                        <div class="col-span-1 flex flex-col items-end">
                            <img class="max-w-full" src="img/team/kelly.png" alt="Kelly W.">
                            <span>Senior Producer</span>
                        </div>
                        <div class="col-span-1 flex flex-col items-end">
                            <img class="max-w-full" src="img/team/zoe.png" alt="Zoe W.">
                            <span>Zoe W.</span>
                        </div>
                    </div>
                    <div class="sm:grid sm:grid-cols-4">
                        <div class="col-span-1 flex flex-col items-end">
                            <img class="max-w-full" src="img/team/angela.png" alt="Angela M.">
                            <span>Social Media</span>
                        </div>
                        <div class="col-span-1 flex flex-col items-end">
                            <img class="max-w-full" src="img/team/harley.png" alt="Harley P.">
                            <span>Lead Artist</span>
                        </div>
                        <div class="col-span-1 flex flex-col items-end">
                            <img class="max-w-full" src="img/team/john.png" alt="John D.">
                            <span>Project Manager</span>
                        </div>
                        <div class="col-span-1 flex flex-col items-end">
                            <img class="max-w-full" src="img/team/julius.png" alt="Julius">
                            <span>Animator</span>
                        </div>
                    </div>
                </li>
                <li id="demo1" class="flex flex-col gap-y-8">
                    <div class="sm:grid sm:grid-cols-4">
                        <div class="col-span-1 flex flex-col items-end">
                            <img class="max-w-full" src="/img/team/ernesto.png" alt="Ernesto">
                            <span>3D artist</span>
                        </div>
                        <div class="col-span-1 flex flex-col items-end">
                            <img class="max-w-full" src="img/team/viktor.png" alt="Viktor L.">
                            <span>Blockchain engineer</span>
                        </div>
                        <div class="col-span-1 flex flex-col items-end">
                            <img class="max-w-full" src="img/team/rosame.png" alt="Rosame">
                            <span>Lead Artist</span>
                        </div>
                        <div class="col-span-1 flex flex-col items-end">
                            <img class="max-w-full" src="img/team/tomas.png" alt="Tomas">
                            <span>Animation</span>
                        </div>
                    </div>
                    <div class="sm:grid sm:grid-cols-4">
                        <div class="col-span-1">&nbsp;
                        </div>
                        <div class="col-span-1 flex flex-col items-end">
                            <img class="max-w-full" src="img/team/andrew.png" alt="Angela s.">
                            <span>Lead Artist</span>
                        </div>
                        <div class="col-span-1 flex flex-col items-end">
                            <img class="max-w-full" src="img/team/jan.png" alt="Jan L.">
                            <span>Animator</span>
                        </div>
                        <div class="col-span-1">&nbsp;
                        </div>
                </li>
            </ul>
        </div>
    </div>

    <div id="team-mobile" class="scrollmenu">
        <div>
            <img src="/img/team/scott.png" alt="Scott O.">
            <span>Founder | Creative Director</span>
        </div>
        <div>
            <img src="img/team/armond.png" alt="Armond A.">
            <span>CFO</span>
        </div>
        <div>
            <img src="img/team/kelly.png" alt="Kelly W.">
            <span>Senior Producer</span>
        </div>
        <div>
            <img src="img/team/zoe.png" alt="Zoe W.">
            <span>Zoe W.</span>
        </div>
        <div>
            <img src="img/team/angela.png" alt="Angela M.">
            <span>Social Media</span>
        </div>
        <div>
            <img src="img/team/harley.png" alt="Harley P.">
            <span>Lead Artist</span>
        </div>
        <div>
            <img src="img/team/john.png" alt="John D.">
            <span>Project Manager</span>
        </div>
        <div>
            <img src="img/team/julius.png" alt="Julius">
            <span>Animator</span>
        </div>
        <div>
            <img src="/img/team/ernesto.png" alt="Ernesto">
            <span>3D artist</span>
        </div>
        <div>
            <img src="img/team/viktor.png" alt="Viktor L.">
            <span>Blockchain engineer</span>
        </div>
        <div>
            <img src="img/team/rosame.png" alt="Rosame">
            <span>Lead Artist</span>
        </div>
        <div>
            <img src="img/team/tomas.png" alt="Tomas">
            <span>Animation</span>
        </div>
        <div>
            <img src="img/team/andrew.png" alt="Angela s.">
            <span>Lead Artist</span>
        </div>
        <div>
            <img src="img/team/jan.png" alt="Jan L.">
            <span>Animator</span>
        </div>
    </div>

</div>
<script>
    $(function() {

        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {

            $("#team").hide();
            $("#team-mobile").show();
        } else {}

        if (jQuery().slippry) {
            jQuery('.wrapper').slippry({
                // elements: '.articles',		
                captions: false
                , auto: false
                , transition: 'horizontal', // fade, horizontal, kenburns, false
                // controls: false,					//controlClass: 'ns-controls',		
                pager: false
            });

        }
    });

</script>
@endsection
