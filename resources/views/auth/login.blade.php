<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            {{-- <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a> --}}
        </x-slot>

        <div class="text-center text-white qb-one mb-6">LOG IN</div>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <!-- Email Address -->
            <div>
                <x-input id="username" class="block mt-1 w-full bg-black border text-white border-gray-500 px-8 py-4 rounded-xl" type="text" name="username" :value="old('username')" placeholder="Username" required autofocus />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-input id="password" class="block mt-1 w-full bg-black border text-white border-gray-500 px-8 py-4 rounded-xl" type="password" name="password" placeholder="Password" required autocomplete="current-password" />
            </div>

            <!-- Remember Me -->
            {{-- <div class="block mt-4">
                <label for="remember_me" class="inline-flex items-center">
                    <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
            </label>
            </div> --}}

            <div class="flex items-center justify-center mt-4">
                {{-- @if (Route::has('password.request'))
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                {{ __('Forgot your password?') }}
                </a>
                @endif --}}

                <x-button class="ml-3 mt-6 bg-gradient px-12 qb-one">
                    {{ __('Enter') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
