<!DOCTYPE html>
<html>
<head>

    <!-- Global site tag (gtag.js) - Google Ads -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-10881855478"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'AW-10881855478');

    </script>

    <meta charset="utf-8">

    <title>A NFT Play-to-earn-game</title>

    <!--http://www.html5rocks.com/en/mobile/mobifying/-->
    <meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1, minimum-scale=1,maximum-scale=1" />

    <!--https://developer.apple.com/library/safari/documentation/AppleApplications/Reference/SafariHTMLRef/Articles/MetaTags.html-->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no">

    <!-- force webkit on 360 -->
    <meta name="renderer" content="webkit" />
    <meta name="force-rendering" content="webkit" />
    <!-- force edge on IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="msapplication-tap-highlight" content="no">

    <!-- force full screen on some browser -->
    <meta name="full-screen" content="yes" />
    <meta name="x5-fullscreen" content="true" />
    <meta name="360-fullscreen" content="true" />

    <!-- force screen orientation on some browser -->
    <meta name="screen-orientation" content="" />
    <meta name="x5-orientation" content="">

    <!--fix fireball/issues/3568 -->
    <!--<meta name="browsermode" content="application">-->
    <meta name="x5-page-mode" content="app">
    <link rel="stylesheet" href="/css/app.css" />
    <link rel="stylesheet" href="/extras/style-desktop.css" />
    <!--<link rel="apple-touch-icon" href=".png" />-->
    <!--<link rel="apple-touch-icon-precomposed" href=".png" />-->

    <!-- <link rel="stylesheet" type="text/css" href="/extras/style-mobile.css" /> -->

    <!--for mobile-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
    <!-- jQuery Modal -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />


    <link rel="icon" href="favicon.ico" />
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
</head>
<body x-on:resize.window.debounce="isMobile = (window.innerWidth < 1024) ? true : false" x-data="{isMobile: (window.innerWidth < 1024) ? true : false, forceHide: false}" x-init="$nextTick(() => {
        forceHide = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 0))? true : false;        
    })">
    @include('layout.menu', ['hideBeforeInit' => true])
    <canvas x-show="!isMobile && !forceHide" id="GameCanvas" oncontextmenu="event.preventDefault()" tabindex="0"></canvas>
    <div x-show="!isMobile && !forceHide" id="splash">
        <div class="progress-bar stripes">
            <span style="width: 0%"></span>
        </div>
    </div>
    <div id="mobile" class="relative" x-show="isMobile || forceHide">
        <div>
            <div id="home">
                <picture>
                    <source media="(max-width: 600px)" sizes="100vw" srcset="img/bg-mobile.png 1x">
                    <source media="(min-width: 601px)" sizes="100vw" srcset="img/bg-ipad.png 1x, img/bg-ipad@2x.png 2x">
                    <img class="img-fluid img-header" src="img/bg-ipad.png" alt="bg signsofthetimes" style="width: 100%;height: auto;">
                </picture>
                <div class="txttop">Be the first to collect one of 5000 P2E NFT’s
                    from Signs of the Times. Created by the award
                    winning animation studio of Animatic Media.
                </div>
                <div class="battle">
                    <a href="">
                        <picture>
                            <source media="(max-width: 600px)" sizes="100vw" srcset="img/btn-protest.png 1x">
                            <source media="(min-width: 601px)" sizes="100vw" srcset="img/btn-protest-ipad.png 1x, img/btn-protest-ipad.png 2x">
                            <img class="img-fluid img-header" src="img/btn-protest-ipad.png" alt="protest" style="width: 100%;height: auto;">
                        </picture>
                    </a>
                    <a href="">
                        <picture>
                            <source media="(max-width: 600px)" sizes="100vw" srcset="img/btn-battle.png 1x">
                            <source media="(min-width: 601px)" sizes="100vw" srcset="img/btn-battle-ipad.png 1x, img/btn-battle-ipad.png 2x">
                            <img class="img-fluid img-header" src="img/btn-battle-ipad.png" alt="battle" style="width: 100%;height: auto;">
                        </picture>
                    </a>
                    <a href="">
                        <picture>
                            <source media="(max-width: 600px)" sizes="100vw" srcset="img/btn-earn.png 1x">
                            <source media="(min-width: 601px)" sizes="100vw" srcset="img/btn-earn-ipad.png 1x, img/btn-earn-ipad.png 2x">
                            <img class="img-fluid img-header" src="img/btn-earn-ipad.png" alt="earn" style="width: 100%;height: auto;">
                        </picture>
                    </a>
                </div>
                <a href="#frmSignup" class="btn" id="signup" rel="modal:open">Sign-up today</a>
                <!-- <img class="" src="img/Red-Camo-Guy-Idle-Cheer.gif" alt="" style="width: 100%;height: auto;position:absolute;top:0;">  -->
            </div>
            <div class="footer absolute text-center text-white left-0 right-0 bottom-[20px] text-[11px] montserrat">SIGNS OF THE TIMES&trade; is a Animatic Media Production<br>Made in Pompano Beach Florida</div>

            <div id="frmSignup" class="modal !absolute top-1/2 left-1/2 -mt-[230px] -ml-[220px]">
                <div class="join">Join the whitelist</div>
                <div>
                    <input type="text" id="name" placeholder="First name">
                    <input type="text" id="email" placeholder="Email address">
                    <span id="error" class="text-white text-[12px]"></span>
                    <button type="submit" class="btn" id="submit">Submit</button>
                </div>
                <div class="txt">* Your email will not be used for any solicitation of any kind.</div>
            </div>
            <div id="success" class="modal !absolute top-1/2 left-1/2 -mt-[230px] -ml-[220px]">
                <div class="join">Success</div>
                <div class="txt">* Your email will not be used for any solicitation of any kind.</div>
            </div>
            <div id="video" class="modal !w-full !max-w-full !p-0 -ml-[40px]">
                <div class="vimeo-full-width pb-[56.25%] px-0 pt-0 relative">
                    <iframe class="absolute top-0 left-0 w-full h-full" src="https://player.vimeo.com/video/699978598" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    <script src="/extras/src/settings.js" charset="utf-8"></script>

    <script src="/extras/main.js" charset="utf-8"></script>
    <script type="text/javascript">
        (function() {
            // open web debugger console
            if (typeof VConsole !== 'undefined') {
                window.vConsole = new VConsole();
            }

            var debug = window._CCSettings.debug;
            var splash = document.getElementById('splash');
            splash.style.display = 'block';

            function loadScript(moduleName, cb) {
                function scriptLoaded() {
                    document.body.removeChild(domScript);
                    domScript.removeEventListener('load', scriptLoaded, false);
                    cb && cb();
                };
                var domScript = document.createElement('script');
                domScript.async = true;
                domScript.src = moduleName;
                domScript.addEventListener('load', scriptLoaded, false);
                document.body.appendChild(domScript);
            }

            loadScript(debug ? '/extras/cocos2d-js.js' : '/extras/cocos2d-js-min.js', function() {
                if (CC_PHYSICS_BUILTIN || CC_PHYSICS_CANNON) {
                    loadScript(debug ? '/extras/physics.js' : '/extras/physics-min.js', window.boot);
                } else {
                    window.boot();
                }
            });

        })();

    </script>
    <script type="text/javascript">
        window.captureEmail = function(email, name) {
            axios.post('/api/mails', {
                email
                , name
            }).then(response => {
                console.log(response.data);
            });
        }

        $(document).ready(function() {
            $("#submit").click(function() {
                var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

                var name = $("#name").val();
                var email = $("#email").val();

                if (name.trim() == "") {
                    $("#error").text("Please enter name.");
                    return;
                }

                if (!testEmail.test(email)) {
                    $("#error").text("Invalid email.");
                    return;
                }

                axios.post('/api/mails', {
                    email
                    , name
                }).then(response => {
                    $("#email").val("");
                    $("#name").val("");

                    $("#frmSignup").modal('hide');
                    $("#success").modal('show');
                });
            });
        });

        $(".battle a").click(function() {
            $("#video").modal('show');
            return false;
        });

    </script>
    <script src="/js/app.js"></script>
</body>
</html>
