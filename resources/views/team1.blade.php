<!DOCTYPE html>
<html>
<head>
     <!-- Global site tag (gtag.js) - Google Ads -->
     <script async src="https://www.googletagmanager.com/gtag/js?id=AW-10881855478"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'AW-10881855478');

    </script>
    
    <meta charset="utf-8">

    <title>A NFT Play-to-earn-game</title>

    <!--http://www.html5rocks.com/en/mobile/mobifying/-->
    <meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1, minimum-scale=1,maximum-scale=1" />

    <!--https://developer.apple.com/library/safari/documentation/AppleApplications/Reference/SafariHTMLRef/Articles/MetaTags.html-->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no">

    <!-- force webkit on 360 -->
    <meta name="renderer" content="webkit" />
    <meta name="force-rendering" content="webkit" />
    <!-- force edge on IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="msapplication-tap-highlight" content="no">

    <!-- force full screen on some browser -->
    <meta name="full-screen" content="yes" />
    <meta name="x5-fullscreen" content="true" />
    <meta name="360-fullscreen" content="true" />

    <!-- force screen orientation on some browser -->
    <meta name="screen-orientation" content="" />
    <meta name="x5-orientation" content="">

    <!--fix fireball/issues/3568 -->
    <!--<meta name="browsermode" content="application">-->
    <meta name="x5-page-mode" content="app">
    <link rel="stylesheet" href="/css/slippry.css">
    <link rel="stylesheet" href="/css/app.css" />

    <!--<link rel="apple-touch-icon" href=".png" />-->
    <!--<link rel="apple-touch-icon-precomposed" href=".png" />-->

    <link rel="stylesheet" type="text/css" href="/assets/team/style-mobile.css" />
    <link rel="icon" href="favicon.ico" />
    <style>
        .menu a,
        .menu .fab {
            color: #E53352;
        }

    </style>
</head>
<body x-on:resize.window.debounce="isMobile = (window.innerWidth < 1024) ? true : false" x-data="{isMobile: (window.innerWidth < 1024) ? true : false, forceHide: false}" x-init="$nextTick(() => {
        forceHide = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))? true : false;        
    })">
    @include('layout.menu')

    <canvas x-show="!isMobile && !forceHide" id="GameCanvas" oncontextmenu="event.preventDefault()" tabindex="0"></canvas>
    <div x-show="!isMobile && !forceHide" id="splash">
        <div class="progress-bar stripes">
            <span style="width: 0%"></span>
        </div>
    </div>

    <div x-show="isMobile || forceHide" class="overflow-hidden relative">
        <picture class="">
            <source media="(max-width: 480px)" sizes="100vw" srcset="img/team/bg-mobile.png 1x">
            <source media="(max-width: 1024px)" sizes="100vw" srcset="img/team/bg-ipad.png 1x, img/team/bg-ipad@2x.png 2x">
            <source media="(max-width: 1920px)" sizes="100vw" srcset="img/team/bg.jpg 1x, img/team/bg@2x.jpg 2x">
            <img class="img-fluid img-header" src="img/team/bg.jpg" alt="bg" style="width: 100%;height: auto;">
        </picture>

        <div id="team-mobile" class="scrollmenu">
            <div>
                <img src="/img/team/scott.png" alt="Scott O.">
                <span>Founder | Creative Director</span>
            </div>
            <div>
                <img src="img/team/armond.png" alt="Armond A.">
                <span>CFO</span>
            </div>
            <div>
                <img src="img/team/kelly.png" alt="Kelly W.">
                <span>Senior Producer</span>
            </div>
            <div>
                <img src="img/team/zoe.png" alt="Zoe W.">
                <span>Zoe W.</span>
            </div>
            <div>
                <img src="img/team/angela.png" alt="Angela M.">
                <span>Social Media</span>
            </div>
            <div>
                <img src="img/team/harley.png" alt="Harley P.">
                <span>Lead Artist</span>
            </div>
            <div>
                <img src="img/team/john.png" alt="John D.">
                <span>Project Manager</span>
            </div>
            <div>
                <img src="img/team/julius.png" alt="Julius">
                <span>Animator</span>
            </div>
            <div>
                <img src="/img/team/ernesto.png" alt="Ernesto">
                <span>3D artist</span>
            </div>
            <!-- <div>
                <img src="img/team/viktor.png" alt="Viktor L.">
                <span>Blockchain engineer</span>
            </div> -->
            <div>
                <img src="img/team/rosame.png" alt="Rosame">
                <span>Lead Artist</span>
            </div>
            <div>
                <img src="img/team/tomas.png" alt="Tomas">
                <span>Animation</span>
            </div>
            <div>
                <img src="img/team/andrew.png" alt="Angela s.">
                <span>Lead Artist</span>
            </div>
            <div>
                <img src="img/team/jan.png" alt="Jan L.">
                <span>Animator</span>
            </div>
        </div>

    </div>

    <script src="/assets/team/src/settings.js" charset="utf-8"></script>

    <script src="/assets/team/main.js" charset="utf-8"></script>

    <script type="text/javascript">
        (function() {
            // open web debugger console
            if (typeof VConsole !== 'undefined') {
                window.vConsole = new VConsole();
            }

            var debug = window._CCSettings.debug;
            var splash = document.getElementById('splash');
            splash.style.display = 'block';

            function loadScript(moduleName, cb) {
                function scriptLoaded() {
                    document.body.removeChild(domScript);
                    domScript.removeEventListener('load', scriptLoaded, false);
                    cb && cb();
                };
                var domScript = document.createElement('script');
                domScript.async = true;
                domScript.src = moduleName;
                domScript.addEventListener('load', scriptLoaded, false);
                document.body.appendChild(domScript);
            }

            loadScript(debug ? '/assets/team/cocos2d-js.js' : '/assets/team/cocos2d-js-min.js', function() {
                if (CC_PHYSICS_BUILTIN || CC_PHYSICS_CANNON) {
                    loadScript(debug ? '/assets/team/physics.js' : '/assets/team/physics-min.js', window.boot);
                } else {
                    window.boot();
                }
            });
        })();

    </script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script defer src="/js/slippry.min.js"></script>
    <script>
        $(function() {

            if (jQuery().slippry) {
                jQuery('.wrapper').slippry({
                    // elements: '.articles',		
                    captions: false
                    , auto: false
                    , transition: 'horizontal', // fade, horizontal, kenburns, false
                    // controls: false,					//controlClass: 'ns-controls',		
                    pager: false
                });

            }
        });

    </script>
    <script src="/js/app.js"></script>
</body>
</html>
