<div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0">
    <div>
        {{ $logo }}
    </div>

    <div class="w-full sm:max-w-3xl mt-6 py-4  shadow-md overflow-hidden sm:rounded-xl gradient">
        <div class="w-full sm:max-w-md py-20 mx-auto">
            {{ $slot }}
        </div>

    </div>
</div>
