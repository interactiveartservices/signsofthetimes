@extends('layout.master', ['header' => 'logo'])
@section('header_includes')
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
@endsection

@section('content')
<div class="bg-footer max-w-full relative text-white pb-32" x-data="SelectForm()">
    <div class="max-w-7xl mx-auto flex text-center items-center justify-center pt-32">

        <div class="flex flex-col items-center w-full relative z-1">
            <div class="h-[70px] overflow-hidden">
                <img class="relative" src="/img/menu.png" :class="[currentmenu===1 ? '' : '', currentmenu===2 ? 'hover-2' : '', currentmenu ===3 ? 'hover-3' : '']" />
            </div>
            {{-- <div class="grid grid-cols-3 w-full text-white max-w-[654px] h-[70px] absolute z-10">
                <a class="col-span-1" @click="currentmenu = 1" @mouseover="hovermenu = 1" @mouseleave="hovermenu = currentmenu"><span class="sr-only">Select</span></a>
                <a class="col-span-1" @click="currentmenu = 2" @mouseover="hovermenu = 2" @mouseleave="hovermenu = currentmenu"><span class="sr-only">Personalize</span></a>
                <a class="col-span-1" @click="currentmenu = 3" @mouseover="hovermenu = 3" @mouseleave="hovermenu = currentmenu"><span class="sr-only">Signup</span></a>
            </div> --}}
            <div :class="currentmenu === 1 ? '' : 'hidden'" class="grid grid-cols-2 w-full mt-12 max-w-3xl space-x-4">
                <div class="col-span-1 gradient rounded-xl pb-6 pt-10">
                    <img src="/img/select/guy.png" />
                    <button type="button" @click="formData.character = 'guy';currentmenu = 2" class="bg-gradient px-16 py-2 rounded-lg">SELECT</button>
                </div>
                <div class="col-span-1 gradient rounded-xl pb-6 pt-10">
                    <img src="/img/select/girl.png" />
                    <button type="button" @click="formData.character = 'girl';currentmenu = 2" class="bg-gradient px-16 py-2 rounded-lg">SELECT</button>
                </div>
            </div>
            <span :class="currentmenu === 1 ? '' : 'hidden'" class="montserrat text-sm mt-6">In guest mode you can chat, share and explore our <Br />
                Prelaunch NFT Metaverse
            </span>
            <div :class="currentmenu === 2 ? '' : 'hidden'" class="w-[952px] h-[548px] mt-12">
                <div class="gradient rounded-xl  w-7xl relative h-full">
                    <div class="flex h-full">
                        <div class=" absolute -left-[40px] top-0">
                            <img :src="`/img/select/${formData.character}.png`" class="w-full" />
                        </div>

                        <div class="flex flex-col items-center justify-between w-full pl-20">
                            <span class="montserrat text-lg font-semibold mt-32">Give your avatar a name</span>
                            <input x-model="formData.avatar_name" class="bg-black border border-gray-500 px-12 py-4 rounded-xl w-7/12" type="text" name="guestname" id="guestname" placeholder="Guest name..." />
                            <button type="button" class="bg-gradient px-16 py-2 rounded-lg" @click="currentmenu = 3">NEXT</button>
                            <p class="text-gray-400 pb-6 leading-tight">
                                NFT members will be able to chat, vote, earn experience &amp;<br />
                                form teams in the prelaunch and have a long list of exclusive perks <br>
                                and benefits. <a href="#" class="text-pink-600 font-bold">Learn more</a>

                            </p>
                        </div>
                        <div class="w-10"><a href="#" @click.prevent="currentmenu = 1;formData.character = ''" class="relative text-5xl leading-none right-2">&times;</a></div>
                    </div>
                </div>
            </div>

            <div :class="currentmenu === 3 ? '' : 'hidden'" class="w-[952px] h-[548px] mt-12">
                <div class="gradient rounded-xl w-7xl relative h-full">
                    <div class="flex h-full">
                        <div class=" absolute -left-[40px] top-0">
                            <img :src="`/img/select/${formData.character}.png`" class="w-full" />
                        </div>

                        <div class="flex flex-col items-center justify-evenly w-full">
                            <span class="montserrat text-lg font-semibold mt-20">Sign-up</span>
                            <input x-model="formData.firstname" class="bg-black border border-gray-500 px-6 py-4 rounded-xl w-2/5" type="text" name="guestname" id="guestname" placeholder="First name" />
                            <input x-model="formData.email" class="bg-black border border-gray-500 px-6 py-4 rounded-xl w-2/5" type="text" name="guestname" id="guestname" placeholder="Email" />
                            <button type="button" @click="submitForm" class="bg-gradient px-16 py-2 rounded-lg">Next</button>
                            <p class="montserrat font-size-15">
                                NFT members will be able to chat, vote, earn experience &amp;<br />
                                form teams in the prelaunch and have a long list of exclusive perks <br>
                                and benefits. <a href="#" class="text-pink-600 font-bold">Learn more</a>

                            </p>
                        </div>
                        <div class="w-10"><a href="#" @click.prevent="currentmenu = 2" class="relative text-5xl leading-none right-2">&times;</a></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="pb-12">&nbsp;</div>
    <span class="montserrat text-xs absolute bottom-[20px] text-center w-full">SIGNS OF THE TIMES&trade; is a Animatic Media Production<br />
        Made in Pompano Beach Florida</span>
</div>

<script>
    const SelectForm = () => {
        return {
            hovermenu: 1
            , currentmenu: 1
            , formData: {
                character: 'guy'
                , avatar_name: ''
                , email: ''
                , firstname: ''
            }
            , submitForm() {
                axios.post('/api/sotusers', this.formData).then(response => {
                    console.log(response.data);
                }).catch((error) => {
                    console.log(error.response.data.message);
                });
            }
        }
    }

</script>
@endsection
