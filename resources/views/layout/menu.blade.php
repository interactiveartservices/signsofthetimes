<div id="header-menu" class="absolute {{ !empty($hideBeforeInit) ? $hideBeforeInit ? 'hidden' : '' : '' }} w-full bg-transparent z-10" x-data="{ open: false }">
    <div class="menu max-w-full mx-auto pl-0 sm-max:pl-4 px-4 -mt-[75px]">
        <div class="flex justify-between">
            <div class="flex space-x-7">
                <a href="/" class="flex items-center py-4 px-2">
                    <img src="/img/logo.png" alt="Logo" class="logo lg-max:w-[55%] lg-max:m-0 bg-transparent mr-2">
                </a>
                <!-- Primary Navbar items -->
                <div class="primary hidden lg-max:hidden md:flex items-center space-x-1 text-menu kurhosia text-lg sm:gap-x-8">
                    <a href="#">ROADMAP</a>
                    <a href="/team">TEAM</a>
                    <a href="#">MISSION</a>
                </div>
            </div>
            <!-- Secondary Navbar items -->
            <div class="social hidden lg-max:hidden md:flex items-center gap-x-10 text-lg" style="margin-right: 75px;">
                <a href="https://mobile.twitter.com/signsoftimesnft" class="p-5"><i class="not-italic fab fa-twitter text-black"></i></a>
                <a href="https://www.instagram.com/signsoftimesnft/" class="p-5"><i class="not-italic fab fa-instagram text-black"></i></a>
                <a href="#" class="p-5"><i class="not-italic fab fa-youtube text-black"></i></a>
                <a href="https://www.tiktok.com/@signsoftimesnft" class="p-5"><i class="not-italic fab fa-tiktok text-black"></i></a>
            </div>
            <!-- Mobile menu button -->
            <div class="flex xl:hidden items-center">
                <button x-on:click="open = ! open" class="outline-none mobile-menu-button text-white">
                    <svg class=" w-6 h-6 " fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor">
                        <path d="M4 6h16M4 12h16M4 18h16"></path>
                    </svg>
                </button>
            </div>
        </div>
    </div>
    <!-- mobile menu -->
    <div x-bind:class="! open ? 'hidden' : ''" class="mobile-menu">

        <div class="menu max-w-full mx-auto px-4 -mt-[70px]">
            <div class="flex space-x-7">
                <a href="/" class="flex items-center py-4 px-2">
                    <img src="/img/logo.png" alt="Logo" class="logo lg-max:w-[55%] lg-max:m-0 bg-transparent mr-2">
                </a>
            </div>
        </div>
        <div class="close" x-on:click="open = !open">&times;</div>
        <ul class="menu-container">
            <li class="item"><a class="button" href="#"><span>Roadmap</span></a></li>
            <li class="item"><a class="button" href="/team"><span>Team</span></a></li>
            <li class="item"><a class="button" href="#"><span>MISSION</span></a></li>
            <li class="item social">
                <a class="button" href="https://mobile.twitter.com/signsoftimesnft">
                    <span><i class="not-italic fab fa-twitter text-white"></i></span>
                </a>
            </li>
            <li class="item social">
                <a class="button" href="https://www.instagram.com/signsoftimesnft/">
                    <span><i class="not-italic fab fa-instagram text-white"></i></span>
                </a>
            </li>
            <li class="item social">
                <a class="button" href="#">
                    <span><i class="not-italic fab fa-youtube text-white"></i></span>
                </a>
            </li>
            <li class="item social">
                <a class="button" href="https://www.tiktok.com/@signsoftimesnft">
                    <span><i class="not-italic fab fa-tiktok text-white"></i></span>
                </a>
            </li>
        </ul>
    </div>
</div>
