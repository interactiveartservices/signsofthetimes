<div id="header-menu" class="absolute w-full bg-transparent z-10">
    <div class="menu max-w-full mx-auto pl-0 sm-max:pl-4 px-4 -mt-[75px]">
        <div class="flex justify-between">
            <div class="flex space-x-7">
                <a href="/" class="flex items-center py-4 px-2">
                    <img src="/img/logo.png" alt="Logo" class="logo lg-max:w-[55%] lg-max:m-0 bg-transparent mr-2">
                </a> <!-- Primary Navbar items -->
            </div>
        </div>
    </div>
    <!-- mobile menu -->

</div>
