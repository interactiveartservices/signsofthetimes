<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>A NFT Play-to-earn-game</title>
    <link rel="stylesheet" href="/css/app.css" />
    @yield('header_includes')
    @livewireStyles
</head>
<body class="antialiased bg-black dark:bg-black">
    @includeWhen(!empty($header) && $header === 'logo', 'layout.logo')
    @includeUnless(!empty($header), 'layout.menu')
    @yield('content')
    @livewireScripts
    @yield('footer_includes')
    <script src="/js/app.js"></script>
</body>
</html>
