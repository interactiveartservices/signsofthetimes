<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12 text-white">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="text-center text-white qb-one mb-6">ENTER METAVERSE</div>
            <div class="sm:grid sm:grid-cols-5 space-x-4">
                <div class="col-span-1 gradient rounded-xl pb-6 pt-10 relative">
                    <label for="pinkgirl2">
                        <input id="pinkgirl2" type="checkbox" class="form-checkbox absolute right-3 top-2 rounded-sm bg-transparent border checked:border-white checked:text-black" value="">
                        <img src="/img/avatar/pinkgirl2.png" />
                    </label>
                </div>
                <div class="col-span-1 gradient rounded-xl pb-6 pt-10 relative">
                    <label for="redguy">
                        <input id="redguy" class="form-checkbox absolute right-3 top-2 rounded-sm bg-transparent border checked:border-white checked:text-black" type="checkbox" value="">
                        <img src="/img/avatar/redguy.png" />
                    </label>
                </div>
                <div class="col-span-1 gradient rounded-xl pb-6 pt-10 relative">
                    <label for="skeleton">
                        <input id="skeleton" class="form-checkbox absolute right-3 top-2 rounded-sm bg-transparent border checked:border-white checked:text-black" type="checkbox" value="">
                        <img src="/img/avatar/skeleton.png" />
                    </label>
                </div>
                <div class="col-span-1 gradient rounded-xl pb-6 pt-10 relative">
                    <label for="grunge">
                        <input id="grunge" class="form-checkbox absolute right-3 top-2 rounded-sm bg-transparent border checked:border-white checked:text-black" type="checkbox" value="">
                        <img src="/img/avatar/grunge.png" />
                    </label>
                </div>
                <div class="col-span-1 gradient rounded-xl pb-6 pt-10 relative">
                    <label for="hoodie">
                        <input id="hoodie" class="form-checkbox absolute right-3 top-2 rounded-sm bg-transparent border checked:border-white checked:text-black" type="checkbox" value="">
                        <img src="/img/avatar/hoodie.png" />
                    </label>
                </div>
            </div>

            <div class="gradient rounded-xl w-full relative mt-4">
                <div class="flex flex-col items-center space-y-6 py-8">
                    <input x-model="formData.firstname" class="bg-black border border-gray-500 px-6 py-2 rounded-xl w-2/6" type="text" name="guestname" id="guestname" placeholder="Enter sign message here..." />
                    <input x-model="formData.email" class="bg-black border border-gray-500 px-6 py-2 rounded-xl w-2/6" type="text" name="guestname" id="guestname" placeholder="Avatar name.." />
                    <span><button type="button" @click="submitForm" class="bg-gradient px-16 py-2 rounded-lg hover:bg-red-50">ENTER</button></span>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
