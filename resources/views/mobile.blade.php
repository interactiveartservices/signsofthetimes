<!DOCTYPE html>
<html>
<head>
    <title></title>

    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" type="text/css" href="/extras/style-mobile.css" />

    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>

    <!-- jQuery Modal -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

</head>

<body id="mobile">
    <div class="container">
        <div id="home">
            <picture>
                <source media="(max-width: 600px)" sizes="100vw" srcset="img/mobile-bg.jpg 1x">
                <source media="(min-width: 601px)" sizes="100vw" srcset="img/team/bg.jpg 1x, img/team/bg@2x.jpg 2x">
                <img class="img-fluid img-header" src="img/team/bg.jpg" alt="bg" style="width: 100%;height: auto;">
            </picture>
            <a href="#frmSignup" class="btn" id="signup" rel="modal:open">Sign up today</a>
        </div>


        <div class="footer absolute text-center text-white left-0 right-0 bottom-[20px] text-[11px] montserrat">SIGNS OF THE TIMES™ is a Animatic Media Production<br>Made in Pompano Beach Florida</div>

        <div id="frmSignup" class="modal">
            <div class="join">Join the whitelist</div>
            <div>
                <input type="text" id="name" placeholder="First name">
                <input type="text" id="email" placeholder="Email address">
                <button type="submit" class="btn" id="submit">Submit</button>
            </div>
            <div class="txt">* Your email will not be used for any solicitation of any kind.</div>
        </div>
        <div id="success" class="modal">
            <div class="join">Success</div>
            <div class="txt">* Your email will not be used for any solicitation of any kind.</div>
        </div>

        <script type="text/javascript">
            $(document).ready(function() {

                $("#submit").click(function() {
                    var email = $("#email").val();
                    var name = $("#name").val();

                    axios.post('/api/mails', {
                        email
                        , name
                    }).then(response => {
                        console.log(response.data);

                        $("#email").val("");
                        $("#name").val("");

                        $("#frmSignup").modal('hide');
                        $("#success").modal('show');

                    });
                    console.log(email, name);
                });

            });

        </script>

</body>


</html>
