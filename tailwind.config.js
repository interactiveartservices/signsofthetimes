const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php'
    ],

    theme: {
        extend: {
            screens: {
                'sm-max': { raw: '(max-width: 480px)' },
                'mw-800': { raw: '(max-width: 800px)' },
                'lg-max': { raw: '(max-width: 1024px)' }
            },
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans]
            }
        }
    },

    plugins: [require('@tailwindcss/forms'), require('sailui')]
};
